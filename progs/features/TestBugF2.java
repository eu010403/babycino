class TestBugF2 {
    public static void main(String[] a) {
        System.out.println((new Test().f()));
    }
}

class Test {

    public int f() {
        int count;
        boolean isTrue;
        boolean isFalse;
        isTrue = false;
        isFalse = true;
        count = 1;

        if (isTrue || isFalse){
            count = count + 1;
        } else{
        count = 5;
      }
      return count;
    }

}
