class TestBugE2{
// Bug: > has lower predence than &&
    public static void main(String[] arg){

        System.out.println(new Test().T());
    }
}

class Test {

    public int T(){
        int x;
        int y;
        int z;
        x = 2;
        y = 0;
        z = 4;

        while( x > y && z > x){
            y = y +1;
            z = z -1;
        }
        System.out.println(z);
        return y;
    }
}

