class TestBugE1{
// Bug: allow comparison of booleans
    public static void main(String[] arg){
            System.out.println(new Test().E1());
    }
}
class Test {

    public int E1(){
        int x;
        int y;
        int result;

        x = 7;
        y = 8;
        result = 0;
        while ((y > x) > (x > y)){
            result = 1;
        }
        return result;
    }
}
